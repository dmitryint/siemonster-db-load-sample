FROM mysql:5

COPY ./docker-entrypoint-initdb.d /docker-entrypoint-initdb.d
COPY docker-entrypoint.sh /usr/local/bin/

RUN chmod +x /usr/local/bin/docker-entrypoint.sh \
    && ln -fs usr/local/bin/docker-entrypoint.sh /entrypoint.sh

CMD []